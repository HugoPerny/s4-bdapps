<?php

namespace gamepedia\vue;

use gamepedia\modeles\User;
use Slim\Slim;
use gamepedia\modeles\Game;

class VueRest
{

    private $array;
    private $app;

    public function __construct($a){
        $this->array = $a;
        $this->app = Slim::getInstance();
    }

    private function gameid(){
        $plat = $this->array->platforms()->get();

        $tabplat = [];
        if($this->array != null && $plat != null){
            foreach ($plat as $p){
                //$url = $app->urlFor("jeuID",array("id"=>$p->id));
                $href = ["href"=>$this->app->urlFor("platforms",array("id" => $this->array->id))];
                $tabplat[]= ["id"=>$p->id,"name"=>$p->name,"alias"=>$p->alias,"abbreviation"=>$p->abbreviation,"links"=>["self"=>$href]];
            }
            $com = ["href"=>$this->app->urlFor("comm",array("id" => $this->array->id))];
            $char = ["href"=>$this->app->urlFor("characters",array("id" => $this->array->id))];
            $obj = ["game"=>$this->array,"links"=>["comments"=>$com,"characters"=>$char,"platforms"=>$tabplat]];
            $this->app->response->setStatus(200);
            $this->app->response()->headers->set('Content-Type', 'application/json');
            echo json_encode($obj);
        }else{
            $this->app->response->setStatus(404);
        }
    }

    private function collection(){
        $page = $this->app->request->get('page');
        $tabgames = [];

        if($page!= null){
            $this->array = Game::take(200)->offset($page*200)->get();
            $pageprec = $page-1;
            $pagesuiv = $page+1;
            $hrefprev = ["href"=>$this->app->urlFor("collection")."?page=".$pageprec];
            $hrefnext = ["href"=>$this->app->urlFor("collection")."?page=".$pagesuiv];

            foreach ($this->array as $g){
                $url = $this->app->urlFor("jeuID",array("id"=>$g->id));
                $href = ["href"=>$url];
                $tabgames[]= ["game"=>["id"=>$g->id,"name"=>$g->name,"alias"=>$g->alias,"deck"=>$g->deck],"links"=>["self"=>$href]];
            }

            $obj = ["games"=>$tabgames,"links"=>["prev"=>$hrefprev,"next"=>$hrefnext]];
        }else{
            foreach ($this->array as $g){
                $url = $this->app->urlFor("jeuID",array("id"=>$g->id));
                $href = ["href"=>$url];
                $tabgames[]= ["game"=>["id"=>$g->id,"name"=>$g->name,"alias"=>$g->alias,"deck"=>$g->deck],"links"=>["self"=>$href]];
            }
            $obj = ["games"=>$tabgames];
        }


        if($this->array != null){
            $this->app->response->setStatus(200);
            $this->app->response()->headers->set('Content-Type', 'application/json');
            echo json_encode($obj);
        }else{
            $this->app->response->setStatus(404);
        }
    }

    private function commentaires(){
        $comm = $this->array->commentaires()->get();

        $listCom = [];

        foreach ($comm as $c){
            $nomUser = User::where('id', '=', $c->user_id)->first()->nom;
            $listCom[] = ["id" => $c->id, "titre" => $c->titre, "texte" => $c->contenu, "dateCrea" => $c->created_at, "nomUser" => $nomUser];
        }

        $obj = ["comments" => $listCom];

        if($comm != null){
            $this->app->response->setStatus(200);
            $this->app->response()->headers->set('Content-Type', 'application/json');
            echo json_encode($obj);
        }else{
            $this->app->response->setStatus(404);
        }
    }

    private function platforms(){
        $plat = $this->array->platforms()->get();

        $listPlat = [];

        foreach ($plat as $p){
            $listPlat[] = $p;
        }

        $obj = ["platforms" => $listPlat];

        if($plat != null){
            $this->app->response->setStatus(200);
            $this->app->response()->headers->set('Content-Type', 'application/json');
            echo json_encode($obj);
        }else{
            $this->app->response->setStatus(404);
        }

    }

    private function characters(){
        $char = $this->array->characters()->get();

        $listChar = [];

        foreach ($char as $c){
            $listChar[] = ["character"=>["id" => $c->id, "name" => $c->name],"links"=>["self"=>["href"=>$this->app->urlFor("characters",array("id" => $this->array->id))]]];
        }

        $obj = ["characters"=>$listChar];

        if($char != null){
            $this->app->response->setStatus(200);
            $this->app->response()->headers->set('Content-Type', 'application/json');
            echo json_encode($obj);
        }else{
            $this->app->response->setStatus(404);
        }

    }

    public function render($id){
        switch($id){
            case 1 :
                $this->gameid();
                break;
            case 2 :
                $this->collection();
                break;
            case 3 :
                $this->commentaires();
                break;
            case 4 :
                $this->platforms();
                break;
            case 5 :
                $this->characters();
                break;
        }
    }

}