<?php

namespace gamepedia\modeles;
use \Illuminate\Database\Eloquent\Model;

class Character extends Model{

    protected $table = 'character';
    protected $primarykey = 'id';
    public $timestamps = false;

    public function firstGame(){
        return $this->belongsTo('gamepedia\modeles\Game','id');
    }

    public function games(){
        return $this->belongsToMany('gamepedia\modeles\Game','game2character','character_id', 'game_id');
    }

}