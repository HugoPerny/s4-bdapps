<?php

namespace gamepedia\modeles;
use \Illuminate\Database\Eloquent\Model;

class Theme extends Model{

    protected $table = 'theme';
    protected $primarykey = 'id';
    public $timestamps = false;

    public function games(){
        return $this->belongsToMany('gamepedia\modeles\Game','game2theme','theme_id', 'game_id');
    }

}