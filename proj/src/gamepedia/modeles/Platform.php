<?php

namespace gamepedia\modeles;
use \Illuminate\Database\Eloquent\Model;

class Platform extends Model{

    protected $table = 'platform';
    protected $primarykey = 'id';
    public $timestamps = false;

    public function company(){
        return $this->belongsTo('gamepedia\modeles\Company','c_id');
    }

    public function games(){
        return $this->belongsToMany('gamepedia\modeles\Game','game2platform','platform_id', 'game_id');
    }


}