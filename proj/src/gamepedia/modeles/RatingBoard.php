<?php

namespace gamepedia\modeles;
use \Illuminate\Database\Eloquent\Model;

class RatingBoard extends Model{

    protected $table = 'rating_board';
    protected $primarykey = 'id';
    public $timestamps = false;

    public function games(){
        return $this->hasMany('gamepedia\modeles\GameRating','id');
    }

}