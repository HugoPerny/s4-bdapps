<?php

namespace gamepedia\modeles;
use \Illuminate\Database\Eloquent\Model;

class Company extends Model{

    protected $table = 'company';
    protected $primarykey = 'id';
    public $timestamps = false;

    public function developedGames(){
        return $this->belongsToMany('gamepedia\modeles\Game','game_developers','comp_id', 'game_id');
    }

    public function publishedGames(){
        return $this->belongsToMany('gamepedia\modeles\Game','game_publishers','comp_id', 'game_id');
    }

    public function platforms(){
        return $this->hasMany('gamepedia\modeles\Platform','id');
    }
}