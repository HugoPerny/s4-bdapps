<?php

namespace gamepedia\modeles;
use \Illuminate\Database\Eloquent\Model;

class Game extends Model{

    protected $table = 'game';
    protected $primarykey = 'id';
    public $timestamps = false;

    public function developers(){
        return $this->belongsToMany('gamepedia\modeles\Company','game_developers','game_id', 'comp_id');
    }

    public function publishers(){
        return $this->belongsToMany('gamepedia\modeles\Company','game_publishers','game_id', 'comp_id');
    }

    public function themes(){
        return $this->belongsToMany('gamepedia\modeles\Theme','game2theme','game_id', 'theme_id');
    }

    public function genres(){
        return $this->belongsToMany('gamepedia\modeles\Genre','game2genre','game_id', 'genre_id');
    }

    public function ratings(){
        return $this->belongsToMany('gamepedia\modeles\GameRating','game2rating','game_id', 'rating_id');
    }

    public function platforms(){
        return $this->belongsToMany('gamepedia\modeles\Platform','game2platform','game_id', 'platform_id');
    }

    public function characters(){
        return $this->belongsToMany('gamepedia\modeles\Character','game2character','game_id', 'character_id');
    }

    public function firstAppCharacter(){
        return $this->hasMany('gamepedia\modeles\Character','id');
    }

    public function commentaires(){
        return $this->hasMany('gamepedia\modeles\Commentaire','game_id');
    }


}