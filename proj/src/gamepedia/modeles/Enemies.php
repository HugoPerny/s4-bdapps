<?php

namespace gamepedia\modeles;
use Illuminate\Database\Eloquent\Model;

class Enemies extends Model {

    protected $table = 'enemies';
    protected $primarykey = 'id';
    public $timestamps = false;

}