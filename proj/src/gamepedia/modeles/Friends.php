<?php

namespace gamepedia\modeles;
use \Illuminate\Database\Eloquent\Model;
class Friends extends Model{

    protected $table = 'friends';
    protected $primarykey = 'id';
    public $timestamps = false;

}