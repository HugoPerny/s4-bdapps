<?php

namespace gamepedia\modeles;
use \Illuminate\Database\Eloquent\Model;

class Genre extends Model{

    protected $table = 'genre';
    protected $primarykey = 'id';
    public $timestamps = false;


    public function games(){
        return $this->belongsToMany('gamepedia\modeles\Game','game2genre','genre_id', 'game_id');
    }
}
