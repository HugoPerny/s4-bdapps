<?php

namespace gamepedia\modeles;
use \Illuminate\Database\Eloquent\Model;

class User extends Model{

    protected $table = 'user';
    protected $primarykey = 'id';
    public $timestamps = false;

    public function commentaires(){
        return $this->hasMany('gamepedia\modeles\Commentaire','user_id');
    }

}