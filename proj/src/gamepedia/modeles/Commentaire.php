<?php

namespace gamepedia\modeles;
use \Illuminate\Database\Eloquent\Model;

class Commentaire extends Model{

    protected $table = 'commentaire';
    protected $primarykey = 'id';
    public $timestamps = true;


    public function game(){
        return $this->belongsTo('gamepedia\modeles\Game','id');
    }

    public function user(){
        return $this->belongsTo('gamepedia\modeles\User','id');
    }
}