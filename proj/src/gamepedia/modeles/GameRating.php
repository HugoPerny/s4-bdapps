<?php

namespace gamepedia\modeles;
use \Illuminate\Database\Eloquent\Model;

class GameRating extends Model{

    protected $table = 'game_rating';
    protected $primarykey = 'id';
    public $timestamps = false;

    public function board(){
        return $this->belongsTo('gamepedia\modeles\RatingBoard','rating_board_id');
    }

    public function games(){
        return $this->belongsToMany('gamepedia\modeles\Game','game2rating','rating_id', 'game_id');
    }

}