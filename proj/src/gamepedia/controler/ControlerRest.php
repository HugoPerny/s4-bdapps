<?php

namespace gamepedia\controler;
use gamepedia\modeles\Game;
use gamepedia\vue\VueRest;

class ControlerRest
{

    public function gameid($id){
        $q = Game::where('id','=',$id)->first();
        $vue = new VueRest($q);
        $vue->render(1);
    }

    public function collection(){
        $q = Game::take(200)->get();
        $vue = new VueRest($q);
        $vue->render(2);
    }

    public function commentaire($id){
        $q = Game::where('id','=',$id)->first();
        $vue = new VueRest($q);
        $vue->render(3);
    }

    public function platforms($id){
        $q = Game::where('id','=',$id)->first();
        $vue = new VueRest($q);
        $vue->render(4);
    }

    public function characters($id){
        $q = Game::where('id','=',$id)->first();
        $vue = new VueRest($q);
        $vue->render(5);
    }

}