<?php

require_once '../../vendor/autoload.php';
use gamepedia\controler\ControlerRest;
use Illuminate\Database\Capsule\Manager as DB;

$db = new DB();
$array = parse_ini_file('../../src/conf/conf.ini');
$db->addConnection($array);
$db->setAsGlobal();
$db->bootEloquent();
$app = new \Slim\Slim();

$app->get('/api/games/:id', function ($id){
    $contr = new ControlerRest();
    $contr->gameid($id);

})->name("jeuID");

$app->get('/api/games', function (){
    $contr = new ControlerRest();
    $contr->collection();

})->name("collection");

$app->get('/api/games/:id/comments', function ($id){
    $contr = new ControlerRest();
    $contr->commentaire($id);
})->name("comm");

$app->get('/api/games/:id/platforms', function ($id){
    $contr = new ControlerRest();
    $contr->platforms($id);
})->name("platforms");

$app->get('/api/games/:id/characters', function ($id){
    $contr = new ControlerRest();
    $contr->characters($id);
})->name("characters");


$app->run();