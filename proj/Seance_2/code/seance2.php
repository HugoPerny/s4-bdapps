<?php
/**
 * Created by PhpStorm.
 * User: Jeremy
 * Date: 08/03/2017
 * Time: 13:20
 */

use \gamepedia\modeles\Game;
use \gamepedia\modeles\Company;
use \gamepedia\modeles\GameRating;
use \gamepedia\modeles\Genre;

class Seance2{

   public static function perso12342(){
        $game12342 = Game::where('id','=',12342)->first();
        $perso = $game12342->characters()->select('name','deck')->get();
        return $perso;
    }

    public static function persoMario(){
        $gameMario = Game::where('name','like','Mario%')->get();
        foreach ($gameMario as $g){
            $perso = $g->characters()->select('name')->get();
            foreach ($perso as $p){
                $aff = $p->name." | ";
                echo $aff;
            }
        }
    }

    public static function jeuxSony(){
        $sony = Company::where('name','like','%Sony%')->get();
        foreach ($sony as $s){
            $games = $s->developedGames()->get();
            foreach($games as $g){
                $aff = ($g->name." | ");
                echo($aff);
            }
        }

    }

    public static function ratingMario(){
        $games = Game::where('name', 'like', "%Mario%")->get();
        foreach($games as $g){
            $rating = $g->ratings()->get();
            foreach ($rating as $r){
                $aff = ($r->name." | ");
                echo($aff);
            }
        }
    }

    public static function plusTroisPersonnages(){
        $njeux = Game::where('name','like','Mario%')->has('characters', '>=', 3)->get();
        return $njeux;
    }

    public static function ratingTroisPlus(){
        $njeux = Game::where('name','like','Mario%')->whereHas('ratings', function($query){
            $query->where('name','like','%3+%');
        })->get();
        return $njeux;
    }

    public static function companyInc(){
        $njeux = Game::where('name','like','Mario%')->whereHas('ratings', function($query){
            $query->where('name','like','%3+%');
        })->whereHas('publishers', function($query){
            $query->where('name','like','%Inc.%');
        })->get();
        return $njeux;
    }

    public static function ratingCero(){
        $njeux = Game::where('name','like','Mario%')->whereHas('ratings', function($query){
            $query->where('name','like','%3+%');
        })->whereHas('ratings.board',function($query){
            $query->where('name','like','%CERO%');
        })->whereHas('publishers', function($query){
            $query->where('name','like','%Inc.%');
        })->get();
        return $njeux;
    }

    public static function newGenre(){
        $genre = new Genre();
        $genre->save();
        $id = $genre->id;
        Game::find(12)->genres()->attach($id);
        Game::find(56)->genres()->attach($id);
        Game::find(345)->genres()->attach($id);
    }

}