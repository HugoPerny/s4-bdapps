<?php

use \gamepedia\modeles\Game;
use \gamepedia\modeles\Company;
use \gamepedia\modeles\Platform;
class Seance1{

    public static function jeuxMario(){
        $jeux = Game::where("name","like","%Mario%")->get();
        return $jeux;
    }

    public static function companyJapon(){
        $companies = Company::where("location_country","=","Japan")->get();
        return $companies;
    }

    public static function platformBase(){
        $platforms = Platform::where("install_base",">=","10000000")->get();
        return $platforms;
    }

    public static function jeuxAPartir(){
        $games = Game::skip(21173)->take(442)->get();
        return $games;
    }


    public static function jeuxPagine500(){
        $games = Game::select('name','deck')->take(500)->offset(100*(500-1))->set();
        return $games;
    }
}