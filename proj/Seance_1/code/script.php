<?php

require_once '../../vendor/autoload.php';
require_once 'Seance1.php';
use Illuminate\Database\Capsule\Manager as DB;

$db = new DB();
$array = parse_ini_file('../../src/conf/conf.ini');
$db->addConnection($array);
$db->setAsGlobal();
$db->bootEloquent();

$jeux = Seance1::jeuxMario();
foreach ($jeux as $j) {
    $aff = $j->name;
    $aff = $aff . "\n";
    print($aff);
}

print("\n ---------------------------------------------------------------------- \n");

$companies = Seance1::companyJapon();
foreach ($companies as $c) {
    $aff = $c->name;
    $aff = $aff . "\n";
    print($aff);
}

print("\n ---------------------------------------------------------------------- \n");

$platforms = Seance1::platformBase();
foreach ($platforms as $p) {
    $aff = $p->name;
    $aff = $aff . "\n";
    print($aff);
}

$games = Seance1::jeuxAPartir();
foreach ($games as $g){
    $aff = $g->name;
    $aff = $aff . "\n";
    print($aff);
}


$gamespag = Seance1::jeuxPagine500();