<?php
/**
 * Created by PhpStorm.
 * User: Jeremy
 * Date: 08/03/2017
 * Time: 13:20
 */

use \gamepedia\modeles\Game;
use \gamepedia\modeles\Company;
use \gamepedia\modeles\GameRating;

class Seance3
{

    public static function listJeux()
    {
        $time_start = microtime(true);
        $i = 10000; //Game::get()->count();
       for($j = 0; $j<=$i; $j += 500) {
            Game::take(500)->offset($j)->get();
        }
        $time_end = microtime(true);

        $time = $time_end - $time_start;

        return $time;
    }

    public static function nomMario()
    {
        $time_start = microtime(true);
        Game::where('name', 'like', "%Mario%")->get();
        $time_end = microtime(true);

        $time = $time_end - $time_start;

        return $time;
    }


    public static function premiereApparitionMario()
    {
        $games = Game::where('name', 'like', "%Mario%")->get();
        foreach ($games as $g) {
            $perso = $g->firstAppCharacter()->get();
            foreach ($perso as $p) {
                $aff = ($p->name . " | ");
                echo($aff);
            }
        }
    }

    public static function persoMario()
    {
        $time_start = microtime(true);
        $gameMario = Game::where('name', 'like', 'Mario%')->get();
        foreach ($gameMario as $g) {
            $g->characters()->select('name')->get();
        }
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        return $time;
    }

    public static function ApparitionDansMario(){
        $games = Game::where('name', 'like', "%Mario%")->with('characters')->get();
        foreach($games as $g){
            $aff = ($g->characters->name." | ");
            echo($aff);
        }
    }

    public static function ratingTroisPlus()
    {
        $time_start = microtime(true);
        Game::where('name', 'like', 'Mario%')->whereHas('ratings', function ($query) {
            $query->where('name', 'like', '%3+%');
        })->get();
        $time_end = microtime(true);

        $time = $time_end - $time_start;

        return $time;
    }

    public static function tempsJeuxContientPerso($name){
        $time_start = microtime(true);
        Game::where('name','like','%'.$name.'%')->get();
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        echo $time."\n";
    }

    public static function tempsCompagniesPays($pays){
        $time_start = microtime(true);
        Company::where('location_country','like',$pays)->get();
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        echo $time."\n";
    }

    public static function tempsJeuxDebutePerso($name){
        $time_start = microtime(true);
        Game::where('name','like',$name.'%')->get();
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        echo $time."\n";
    }

}