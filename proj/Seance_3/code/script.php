<?php


require_once '../../vendor/autoload.php';
require_once 'seance3.php';
use Illuminate\Database\Capsule\Manager as DB;

$db = new DB();
$array = parse_ini_file('../../src/conf/conf.ini');
$db->addConnection($array);
$db->setAsGlobal();
$db->bootEloquent();

 DB::enableQueryLog();
 error_reporting(0);
 $r=0;

 print("\n ------------------------[REQUETE 1]------------------------ \n");

 Seance3::persoMario();
 $r+=affichageLog($r);

 print("\n ------------------------[REQUETE 2]------------------------ \n");
$perso12342 = Seance3::perso12342();
$r+=affichageLog($r);


 print("\n ------------------------[REQUETE 3]------------------------ \n");

 Seance3::premiereApparitionMario();
 $r+=affichageLog($r);

 print("\n ------------------------[REQUETE 4]------------------------ \n");

 Seance3::ApparitionDansMario();
 $r+=affichageLog($r);


print("\n ------------------------[REQUETE 5]------------------------ \n");

 Seance3::jeuxSony();
$r+=affichageLog($r);


 function affichageLog($last){
     $queries = DB::getQueryLog();
     $i=$last;
     for ($i; $i<count($queries); $i++) {
         print("Requete : " . $queries[$i][query] . "\n" . "Binding : ");
         foreach($queries[$i][bindings] as $b){
             print($b . "\n");
         }
     }
     print "Requêtes exécutées : ".$i;
     return $i;
 }


print("\n ------------------------[Sans Index]------------------------ \n");

echo seance3::listJeux()."\n";
echo seance3::nomMario()."\n";
echo seance3::persoMario()."\n";
echo seance3::ratingTroisPlus()."\n";
/*
print("\n ------------------------[Avant Index]------------------------ \n");

Seance3::tempsJeuxDebutePerso('Mario');
Seance3::tempsJeuxDebutePerso('Sonic');
Seance3::tempsJeuxDebutePerso('Zelda');
Seance3::tempsJeuxContientPerso('Mario');
Seance3::tempsCompagniesPays('USA');
*/

print("\n ------------------------[Apres Index]------------------------ \n");

Seance3::tempsJeuxDebutePerso('Pac-Man');
Seance3::tempsJeuxDebutePerso('Donkey Kong');
Seance3::tempsJeuxDebutePerso('Mega Man');
Seance3::tempsJeuxContientPerso('Pac-Man');
Seance3::tempsCompagniesPays('France');
