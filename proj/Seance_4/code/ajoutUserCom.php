<?php

require_once '../../vendor/autoload.php';
use Illuminate\Database\Capsule\Manager as DB;
use gamepedia\modeles\User;
use \gamepedia\modeles\Commentaire;

$db = new DB();
$array = parse_ini_file('../../src/conf/conf.ini');
$db->addConnection($array);
$db->setAsGlobal();
$db->bootEloquent();

$format = 'Y-m-d';
$date = DateTime::createFromFormat($format,'1997-11-03');
$user1 = new User();
$user1->nom = "bronner";
$user1->prenom = "jeremy";
$user1->email = "jeremybronner@gmail.com";
$user1->adresse = "2 boulevard charlemagne 54000 Nancy";
$user1->numero = "0654125487";
$user1->datenaiss = $date;
$user1->save();

$com1 = new Commentaire();
$com1->titre = "nul";
$com1->contenu = "ce jeu est vraiment mal fait, les graphiques sont pourris";
$com1->created_at = date('Y-m-d H:i:s');
$com1->game_id = 12342;
$com1->user_id = 1;

$com2 = new Commentaire();
$com2->titre = "tres bon jeu";
$com2->contenu = "merci ce jeu est top";
$com2->created_at = date('Y-m-d H:i:s');
$com2->game_id = 12342;
$com2->user_id = 1;

$com3 = new Commentaire();
$com3->titre = "a revoir";
$com3->contenu = "sympa mais les missions devraient etre revues";
$com3->created_at = date('Y-m-d H:i:s');
$com3->game_id = 12342;
$com3->user_id = 1;

$date2 = DateTime::createFromFormat($format,'1998-03-10');
$user2 = new User();
$user2->nom = "Blum";
$user2->prenom = "Hermine";
$user2->email = "hermineblum@gmail.com";
$user2->adresse = "2 rue St Nicolas 54000 Nancy";
$user2->numero = "0645254487";
$user2->datenaiss = $date2;
$user2->save();


$com4 = new Commentaire();
$com4->titre = "pas mal";
$com4->contenu = "ca passe le temps";
$com4->created_at = date('Y-m-d H:i:s');
$com4->game_id = 12342;
$com4->user_id = 2;

$com5 = new Commentaire();
$com5->titre = "au top";
$com5->contenu = "ce jeu est devenu mon jeu favori";
$com5->created_at = date('Y-m-d H:i:s');
$com5->game_id = 12342;
$com5->user_id = 2;

$com6 = new Commentaire();
$com6->titre = "pourquoi ?";
$com6->contenu = "quel est l'interet de ce jeu?";
$com6->created_at = date('Y-m-d H:i:s');
$com6->game_id = 12342;
$com6->user_id = 2;

$com1->save();
$com2->save();
$com3->save();
$com4->save();
$com5->save();
$com6->save();