<?php

require_once '../../vendor/autoload.php';
use Illuminate\Database\Capsule\Manager as DB;

// use the factory to create a Faker\Generator instance
$faker = Faker\Factory::create();

$db = new DB();
$array = parse_ini_file('../../src/conf/conf.ini');
$db->addConnection($array);
$db->setAsGlobal();
$db->bootEloquent();

//Users
for ($i=0; $i < 25000; $i++) {
    $nom = $faker->lastName;
    $prenom = $faker->firstName;
    $email = $faker->email  ;
    $adresse = $faker->address ;
    $tel = $faker->e164PhoneNumber ;
    $date = $faker->dateTime ;

    $user = new \gamepedia\modeles\User();
    $user->nom = $nom;
    $user->prenom = $prenom;
    $user->email = $email;
    $user->adresse = $adresse;
    $user->numero = $tel;
    $user->datenaiss = $date;
    $user->save();

}

//Commentaires
for ($i=0; $i < 250000; $i++) {
    $titre = $faker->word;
    $contenu = $faker->text($maxNbChars = 50);
    $date = $faker->dateTime  ;

    $com = new \gamepedia\modeles\Commentaire();
    $com->titre = $titre;
    $com->contenu = $contenu;
    $com->created_at = $date;
    $com->game_id = $faker->numberBetween($min = 1, $max = 40000);
    $com->user_id = $faker->numberBetween($min = 1, $max = 24000);
    $com->save();

}
